import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultComponent} from './default.component';
import {DoctorDashboardComponent} from '../../modules/doctor-dashboard/doctor-dashboard.component';
import {RouterModule} from '@angular/router';
import {PatientsComponent} from '../../modules/patients/patients.component';
import {DonorsComponent} from '../../modules/donors/donors.component';
import {SharedModule} from '../../shared/shared.module';
import {LineChartComponent} from '../../modules/doctor-dashboard/line-chart/line-chart.component';
import {ChartsModule} from 'ng2-charts';
import {PieChartComponent} from '../../modules/doctor-dashboard/pie-chart/pie-chart.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import {InfoComponent} from '../../modules/patients/info/info.component';
import {DInfoComponent} from '../../modules/donors/d-info/d-info.component';
import {LoginComponent} from '../../modules/login/login.component';
import {FindADonorComponent} from '../../modules/looking-for-a-donor/find-a-donor.component';
import {InfoAccardionComponent} from '../../modules/info-accardion/info-accardion.component';
import {HaveADonorComponent} from '../../modules/have-a-donor/have-a-donor.component';
import {ViewResultsComponent} from '../../modules/have-a-donor/view-results/view-results.component';
import {DcdDashboardComponent} from '../../modules/dcd-dashboard/dcd-dashboard.component';
import {PatientRequestsComponent} from '../../modules/dcd-dashboard/patient-requests/patient-requests.component';
import {DoctorsComponent} from '../../modules/doctors/doctors.component';
import {TypeAAccardionComponent} from '../../modules/type-a-accardion/type-a-accardion.component';
import {TypeADashboardComponent} from '../../modules/type-a-dashboard/type-a-dashboard.component';
import {TypeBDashboardComponent} from '../../modules/type-b-dashboard/type-b-dashboard.component';
import {PatientInfoAccardionComponent} from '../../modules/patient-info-accardion/patient-info-accardion.component';
import {TypeBInfoComponent} from '../../modules/type-b-info/type-b-info.component';
import {DonorInfoAccardionComponent} from '../../modules/donor-info-accardion/donor-info-accardion.component';



@NgModule({
  declarations: [
    DefaultComponent,
    DoctorDashboardComponent,
    PatientsComponent,
    DonorsComponent,
    LineChartComponent,
    PieChartComponent,
    InfoComponent,
    DInfoComponent,
    LoginComponent,
    FindADonorComponent,
    InfoAccardionComponent,
    HaveADonorComponent,
    ViewResultsComponent,
    DcdDashboardComponent,
    PatientRequestsComponent,
    DoctorsComponent,
    TypeAAccardionComponent,
    TypeADashboardComponent,
    TypeBDashboardComponent,
    PatientInfoAccardionComponent,
    TypeBInfoComponent,
    DonorInfoAccardionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    ChartsModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
  ]
})
export class DefaultModule { }
