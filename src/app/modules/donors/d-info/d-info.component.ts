import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {Patient} from '../../../objects/patient';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-d-info',
  templateUrl: './d-info.component.html',
  styleUrls: ['./d-info.component.css']
})
export class DInfoComponent implements OnInit {
  patient: any = null;
  donor: any = null;
  constructor( private route: ActivatedRoute) {
    if (this.route.snapshot.queryParams.CAN_ABO) {
      this.patient = this.route.snapshot.queryParams;
    } else if (this.route.snapshot.queryParams.DON_ABO) {
      this.donor = this.route.snapshot.queryParams;
    }
    // this.patient = this.route.snapshot.queryParams;
  }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

  openDialog() {
    Swal.fire({
      // icon: 'error',
      // title: 'Leave A Comment',
      // text: 'Here you can leave a comment about this patient.',
      html: '<h1>Leave A Comment</h1>' +
        '<p style="font-size: 18px">Here you can leave a comment about this person</p>' +
        '<textarea style="font-size: 14px" id="text" cols="100" rows="16"></textarea>',
      confirmButtonText: 'Send Comment',
      width: '800px',
      showCloseButton: true,
      // --------------^-- define html element with id
      // html: true,
      // footer: '<a href>Why do I have this issue?</a>'
    });
  }
}
