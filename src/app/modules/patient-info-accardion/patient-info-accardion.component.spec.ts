import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientInfoAccardionComponent } from './patient-info-accardion.component';

describe('PatientInfoAccardionComponent', () => {
  let component: PatientInfoAccardionComponent;
  let fixture: ComponentFixture<PatientInfoAccardionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientInfoAccardionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientInfoAccardionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
