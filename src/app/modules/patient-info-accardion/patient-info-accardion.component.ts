import {Component, Input, OnInit} from '@angular/core';
import {bloodType, diabetesType, gender, IgG_IgM_EBV_HIV_HCV, medicalCondition, previousMalignancies} from '../../objects/patient';

@Component({
  selector: 'app-patient-info-accardion',
  templateUrl: './patient-info-accardion.component.html',
  styleUrls: ['./patient-info-accardion.component.css']
})
export class PatientInfoAccardionComponent implements OnInit {
  @Input() cardTitle;
  @Input() patient;
  options: string[] = [];
  mgender = false;
  fgender = false;

  constructor() {
    if (this.patient) {
      switch (this.patient.CAN_GENDER) {
        case 'Female':
          this.fgender = true;
          break;

        case 'Male':
          this.mgender = true;
          break;
      }
    } else {
      this.patient = {
        firstName: 'Jaydon',
        lastName: 'Lee',
        CAN_GENDER: gender.female,
        CAN_ABO: bloodType.O,
        REC_MED_COND: medicalCondition.NHosp,
        REC_BMI: 27.3465,
        REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
        REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.UnDtr,
        REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
        REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
        REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
        REC_MALIG: previousMalignancies.no,
        REC_CREAT: 11.36,
        REC_A1: 2,
        REC_A2: 3,
        REC_B1: 51,
        REC_B2: 60,
        REC_DR1: 8,
        REC_DR2: 11,
        CAN_DIAB_TY: diabetesType.no,
        CAN_TOT_ALBUMIN: 3.8,
        REC_AGE: 24.16666667,
      };
    }
    for (let i = 0; i < 19; i++) {
    this.options.push(i.toString());
    }
    if (typeof this.options !== 'number') {
      this.options.push('0101');
    }
    this.options.push('0102');
    this.options.push('103');
    this.options.push('0301');
    this.options.push('0302');
    this.options.push('0401');
    this.options.push('0402');
    this.options.push('0404');
    this.options.push('0405');
    this.options.push('0407');
    this.options.push('0901');
    this.options.push('0902');
    this.options.push('1101');
    this.options.push('1104');
    this.options.push('1201');
    this.options.push('1202');
    this.options.push('1301');
    this.options.push('1303');
    this.options.push('1401');
    this.options.push('1402');
    this.options.push('1403');
    this.options.push('1404');
    this.options.push('1501');
    this.options.push('1502');
    this.options.push('1503');
    this.options.push('1601');
    this.options.push('1602');
  }

  ngOnInit(): void {
    // console.log(this.patient.REC_A1);
    // console.log('Female');
  }
}
