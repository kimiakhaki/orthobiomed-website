import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DcdDashboardComponent } from './dcd-dashboard.component';

describe('DcdDashboardComponent', () => {
  let component: DcdDashboardComponent;
  let fixture: ComponentFixture<DcdDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DcdDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DcdDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
