import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeADashboardComponent } from './type-a-dashboard.component';

describe('TypeADashboardComponent', () => {
  let component: TypeADashboardComponent;
  let fixture: ComponentFixture<TypeADashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeADashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeADashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
