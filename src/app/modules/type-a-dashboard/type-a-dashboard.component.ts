import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import * as Chart from 'chart.js';
import {TypeAAccardionComponent} from '../type-a-accardion/type-a-accardion.component';


@Component({
  selector: 'app-type-a-dashboard',
  templateUrl: './type-a-dashboard.component.html',
  styleUrls: ['./type-a-dashboard.component.css']
})
export class TypeADashboardComponent implements OnInit {
  dialysisBoolean: boolean = null;
  predictionYears: number = 1;
  // public barChartData: ChartDataSets[] = [
  //   { data: [ 5, 2, 90], label: 'Percent'},
  // ];
  public barChartData: ChartDataSets[] = [{
    data: [5, 2, 90],
    // categoryPercentage: 20
    // barPercentage: 25
  }];
  public barChartLabels: Label[] = ['Find a deceased donor', 'Died or become too sick for transplant', 'Still waiting for a donor'];
  public barChartOptions: (ChartOptions & { annotation: any }) = {
    aspectRatio: 1.5,
    plugins: {
      datalabels: {
        anchor: 'center',
        color: '#fff',
        font: {
          weight: 'bold',
          size: 18,
        },
      },
      formatter: (value, ctx) => {
        console.log(value);
        // let sum = 0;
        const dataArr = ctx.chart.data.datasets[0].data + '%';
        // dataArr.map(data => {
        //   sum += data;
        // });
        // const percentage = value + '(' + (value * 100 / sum).toFixed(2) + '%)';
        return dataArr;
      },
    },
    legend: {
      display: false,
      labels: {
        // This more specific font property overrides the global property
        fontColor: 'black',
        fontSize: 18,
      }
    },
    responsive: true,
    scales: {
      xAxes: [{
        stacked: false,
        ticks: {
          sampleSize: 2,
          stepSize: 50,
          beginAtZero: true,
          autoSkip: false,
          maxRotation: 7,
          minRotation: 0,
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Probability',
          fontStyle: 'bold',
        },
        ticks: {
          stepSize: 20,
          max: 100,
          min: 0,
          beginAtZero: true,
        }
      }]
    },
    annotation: {
    },
  };
  public barChartColors: Color[] = [
    { // red
      backgroundColor: [
        '#1daf9a',
        '#94b854',
        '#1b1e25',
      ],
    },
  ];
  public barChartLegend = true;
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [pluginAnnotations];

  // @ViewChild(TypeAAccardionComponent) child;
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  constructor() {
    // this.dialysisBoolean = this.child.dialysisBoolean;
  }

  ngOnInit(): void {
  }
  public getData(value): void {
    console.log(value); // welcome to stackoverflow!
  }
  updateTextInput() {
    (<HTMLInputElement>document.getElementById('textInput')).value = (<HTMLInputElement>document.getElementById('prediction')).value + ' years';
    if (this.predictionYears === 1) {
      this.barChartData[0].data = [8, 2, 90];
    } else if (this.predictionYears === 2) {
      this.barChartData[0].data = [15, 5, 80];
    } else if (this.predictionYears === 3) {
      this.barChartData[0].data = [18, 9, 73];
    } else if (this.predictionYears === 4) {
      this.barChartData[0].data = [20, 11, 69];
    } else if (this.predictionYears === 5) {
      this.barChartData[0].data = [25, 16, 58];
    }
  }
}
