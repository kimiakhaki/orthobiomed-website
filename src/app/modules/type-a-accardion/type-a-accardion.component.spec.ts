import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAAccardionComponent } from './type-a-accardion.component';

describe('TypeAAccardionComponent', () => {
  let component: TypeAAccardionComponent;
  let fixture: ComponentFixture<TypeAAccardionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeAAccardionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAAccardionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
