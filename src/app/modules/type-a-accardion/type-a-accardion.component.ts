import {Component, OnInit, Output} from '@angular/core';
import {Province} from '../../objects/province';
import {strict} from 'assert';
import {EventEmitter} from 'events';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-type-a-accardion',
  templateUrl: './type-a-accardion.component.html',
  styleUrls: ['./type-a-accardion.component.css']
})
export class TypeAAccardionComponent implements OnInit {
  collapseOne: boolean = true;
  collapseTwo: boolean = false;
  collapseThree: boolean = false;
  provinces: Province[] = [{
    title: 'British Columbia',
    centers: ["BC Children's Hospital (Vancouver)", "St. Paul's Hospital (Vancouver)", 'Vancouver General Hospital (Vancouver)']
  }, {
    title: 'Alberta',
    centers: ["Alberta Children's Hospital (Calgary)", 'Southern Alberta Renal Program - SARP (Calgary)', 'Northern Alberta Renal Program - SARP (Calgary)']
  }, {
    title: 'Saskatchewan',
    centers: ["St. Paul's Hospital (Saskatoon)"]
  }, {
    title: 'Manitoba',
    centers: ["Children's Hospital Of Winnipeg (Winnipeg)", 'Health Sciences Centre (Winnipeg)']
  }, {
    title: 'Ontario',
    centers: ['London Health Sciences Centre (London)', "St. Joseph's Health Care System (Hamilton)",
      'Hospital For Sick Children (Toronto)', "St. Michael's hospital (Toronto)", 'Toronto General Hospital (Toronto)',
      'Kingston General Hospital (Kingston)', 'The Ottawa Hospital (Ottawa)']
  }, {
    title: 'Quebec',
    centers: ["C.H de l'université de Montréal. Notre Dame (Montreal)", "C.H de l'université de Montréal. St.-Luc (Montreal)",
      'Hôpital Maisonneuve-Rosemont (Montreal)', 'Hôpital Ste-Justine (Montreal)', 'Institut de Cardiologie de Montréal (Montreal)',
    "Montreal Children's Hospital (Montreal)", 'Royal Victoria Hospital (Montreal)', 'C.H universitaire de Sherbrooke (Sherbrooke)',
    'C.H universitaire de Québec (Quebec)', 'Institut Universitaire De Cardiologie (Sainte-Foy)']
  }, {
    title: 'Nova Scotia',
    centers: ['IWK Health Centre (Halifax)', 'Queen Elizabeth II Health Sciences Centre (Halifax)']
  }];
  centers: string[] = [];
  antiBody = false;
  dialysis: string;
  dialysisBoolean: boolean = null;
  @Output() public getUserData = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  getCenters($event) {
    this.centers = $event.target.value.split(',');
    // console.log(this.centers);
  }

  show() {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data ...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
      willClose: () => {
        if (this.dialysis === 'On Dialysis') {
          this.dialysisBoolean = true;
        } else if (this.dialysis === 'Not on Dialysis yet') {
          this.dialysisBoolean = false;
        }
      }
    });
  }

  expendedValue(no: number) {
    if (no === 1) {
      this.collapseOne = true;
      this.collapseTwo = false;
      this.collapseThree = false;
    } else if (no === 2) {
      this.collapseOne = false;
      this.collapseTwo = true;
      this.collapseThree = false;
    } else if (no === 3) {
      this.collapseOne = false;
      this.collapseTwo = false;
      this.collapseThree = true;
    }
  }
}
