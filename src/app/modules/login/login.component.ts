import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  password: string = null;
  username = '';
  swal: any;
  constructor(private router: Router) {
    this.swal = Swal;

  }

  ngOnInit(): void {
  }
  login() {
    if (this.password && this.username) {
      Swal.fire({
        title: 'Please Wait',
        html: 'Logging in ...',
        timer: 1000,
        timerProgressBar: false,
        willOpen: () => {
          Swal.showLoading();
        },
        willClose: () => {
          if (this.password === '1234') {
            this.router.navigateByUrl('/doctor-dashboard');
            localStorage.setItem('role', '1');
          } else if (this.password === '4567') {
            this.router.navigateByUrl('/type-a-dashboard');
            localStorage.setItem('role', '2');
          } else if (this.password === '7891') {
            this.router.navigateByUrl('/type-b-dashboard');
            localStorage.setItem('role', '3');
          }
          else {
            Swal.fire('Your Password Is Incorrect!' , '', 'error');
          }
        }
      });
    } else if (!this.username) {
      Swal.fire('Please enter your username!' , '', 'warning');
    } else if (!this.password) {
      Swal.fire('Please enter your password!' , '', 'warning');
    }


  }
}
