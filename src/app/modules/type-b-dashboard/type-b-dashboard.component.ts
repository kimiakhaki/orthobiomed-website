import {Component, OnInit, ViewChild} from '@angular/core';
import {ChartData, ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import {TypeAAccardionComponent} from '../type-a-accardion/type-a-accardion.component';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import Swal from "sweetalert2";


@Component({
  selector: 'app-type-b-dashboard',
  templateUrl: './type-b-dashboard.component.html',
  styleUrls: ['./type-b-dashboard.component.css']
})
export class TypeBDashboardComponent implements OnInit {
  outCome: string;
  public barChartData: ChartDataSets[] = [
    {data: [ ], order: 2, datalabels: {display: false} },
    {data: [ ], type: 'line',
    order: 1, fill: false, borderWidth: 4, datalabels: {display: false}}];
  public barChartLabels: Label[] = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
  '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];
  public barChartOptions: (ChartOptions & { annotation: any }) = {
    aspectRatio: 1.5,
    plugins: {
      datalabels: {
        anchor: 'center',
        color: '#fff',
        font: {
          weight: 'bold',
          size: 18,
        },
      },
    },
    legend: {
      display: false,
      labels: {
        fontColor: 'black',
        fontSize: 18,
      }
    },
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes:   [{
        scaleLabel: {
          display: true,
          labelString: 'Survival Time (Years)'

        },
        // labels: ['jsdyhuihdisd'],
        stacked: false,
        ticks: {
          // display: false,
          sampleSize: 2,
          stepSize: 10,
          beginAtZero: true,
          autoSkip: true,
          maxRotation: 7,
          minRotation: 0,
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Survival Rate (Percentage)'

        },
        // labels: ['sdasdsad'],
        ticks: {
          stepSize: 10,
          beginAtZero: true,
        }
      }]
    },
    annotation: {
    },
  };
  public barChartColors: Color[] = [
    {backgroundColor: [
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4',
        '#1F77B4'
      ]}, {
      borderColor: 'red'
    }
  ];
  public barChartLegend = true;
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [pluginAnnotations];

  @ViewChild(TypeAAccardionComponent) child;
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  constructor() { }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

  changeData() {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
      willClose: () => {
        if (this.outCome === 'Patient Survival Rate') {
          this.barChartData[0].data = [98, 95, 90, 85, 80, 75, 70, 65, 50, 45, 40, 30, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6,
            5, 4, 3, 2, 1];
          this.barChartData[1].data = [95, 90, 85, 80, 75, 70, 65, 50, 45, 40, 30, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6,
            5, 4, 3, 2, 1, 0];
        } else if (this.outCome === 'Organ Survival Rate') {
          this.barChartData[0].data = [98, 95, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8,
            7, 6, 5, 4, 3, 2, 1];
          this.barChartData[1].data = [95, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7,
            6, 5, 4, 3, 2, 1, 0];
        } else if (typeof this.outCome === undefined ) {
          this.barChartData[0].data = [0];
          this.barChartData[1].data = [0];
        }
      }
    });
  }
}
