import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeBDashboardComponent } from './type-b-dashboard.component';

describe('TypeBDashboardComponent', () => {
  let component: TypeBDashboardComponent;
  let fixture: ComponentFixture<TypeBDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeBDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeBDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
