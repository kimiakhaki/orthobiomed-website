import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HaveADonorComponent } from './have-a-donor.component';

describe('HaveADonorComponent', () => {
  let component: HaveADonorComponent;
  let fixture: ComponentFixture<HaveADonorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HaveADonorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HaveADonorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
