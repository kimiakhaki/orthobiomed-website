import {Component, OnInit} from '@angular/core';
import {
  bloodType,
  diabetesType,
  gender,
  IgG_IgM_EBV_HIV_HCV,
  medicalCondition,
  Patient,
  previousMalignancies
} from '../../objects/patient';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  string = '';
  patients: Patient[] = [{
    firstName: 'Jaydon',
    lastName: 'Lee',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 27.3465,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 11.36,
    REC_A1: 2,
    REC_A2: 3,
    REC_B1: 51,
    REC_B2: 60,
    REC_DR1: 8,
    REC_DR2: 11,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.8,
    REC_AGE: 24.16666667,
  }, {
    firstName: 'Jamison',
    lastName: 'Smith',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 27.6983,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 2.51,
    REC_A1: 1,
    REC_A2: 36,
    REC_B1: 35,
    REC_B2: 57,
    REC_DR1: 1,
    REC_DR2: 7,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 3.8,
    REC_AGE: 43,
  }, {
    firstName: 'Eduardo',
    lastName: 'Murphy',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.A,
    REC_MED_COND: medicalCondition.Hosp,
    REC_BMI: 24.4473,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 4.67,
    REC_A1: 3,
    REC_A2: 24,
    REC_B1: 18,
    REC_B2: 39,
    REC_DR1: 17,
    REC_DR2: 98,
    CAN_DIAB_TY: diabetesType.TI,
    CAN_TOT_ALBUMIN: 4.4,
    REC_AGE: 56.25,
  }, {
    firstName: 'Arlo',
    lastName: 'Hill',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.A,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 34.4491,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 18.4,
    REC_A1: 2,
    REC_A2: 34,
    REC_B1: 37,
    REC_B2: 65,
    REC_DR1: 10,
    REC_DR2: 15,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.2,
    REC_AGE: 45.41666667,
  }, {
    firstName: 'Nile',
    lastName: 'Wilson',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 20.5876,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 10.2,
    REC_A1: 2,
    REC_A2: 26,
    REC_B1: 45,
    REC_B2: 49,
    REC_DR1: 4,
    REC_DR2: 8,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 2.9,
    REC_AGE: 31.08333333,
  }, {
    firstName: 'Lorin',
    lastName: 'Brown',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 30.2278,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 5.01,
    REC_A1: 3,
    REC_A2: 24,
    REC_B1: 7,
    REC_B2: 8,
    REC_DR1: 15,
    REC_DR2: 17,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.9,
    REC_AGE: 70.91666667,
  }, {
    firstName: 'Charley',
    lastName: 'Martin',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.A,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 26.9464,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 3.4,
    REC_A1: 2,
    REC_A2: 98,
    REC_B1: 35,
    REC_B2: 60,
    REC_DR1: 4,
    REC_DR2: 13,
    CAN_DIAB_TY: diabetesType.TI,
    CAN_TOT_ALBUMIN: 3.9,
    REC_AGE: 56.08333333,
  }, {
    firstName: 'Barry',
    lastName: 'Johnson',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 26.4525,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 4.76,
    REC_A1: 30,
    REC_A2: 33,
    REC_B1: 35,
    REC_B2: 42,
    REC_DR1: 11,
    REC_DR2: 13,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.2,
    REC_AGE: 46.58333333,
  }, {
    firstName: 'Meridith',
    lastName: 'Jackson',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 39.6385,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 9.5,
    REC_A1: 30,
    REC_A2: 31,
    REC_B1: 13,
    REC_B2: 51,
    REC_DR1: 1,
    REC_DR2: 4,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.7,
    REC_AGE: 69.08333333,
  }, {
    firstName: 'Janice',
    lastName: 'Hall',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: null,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.Unkwn,
    REC_CREAT: 4.4,
    REC_A1: 1,
    REC_A2: 29,
    REC_B1: 52,
    REC_B2: 35,
    REC_DR1: 1,
    REC_DR2: 7,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 4.5,
    REC_AGE: 69.75,
  }, {
    firstName: 'Kenly',
    lastName: 'Morris',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.A,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 32.9758,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 5.55,
    REC_A1: 2,
    REC_A2: 98,
    REC_B1: 41,
    REC_B2: 62,
    REC_DR1: 1,
    REC_DR2: 13,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.9,
    REC_AGE: 64.66666667,
  }, {
    firstName: 'Carolus',
    lastName: 'Brady',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 31.626,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 14.5,
    REC_A1: 2,
    REC_A2: 30,
    REC_B1: 44,
    REC_B2: 58,
    REC_DR1: 13,
    REC_DR2: 14,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 4.5,
    REC_AGE: 61.91666667,
  }, {
    firstName: 'Lamar',
    lastName: 'Paul',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 25.9188,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 6.58,
    REC_A1: 2,
    REC_A2: 30,
    REC_B1: 7,
    REC_B2: 13,
    REC_DR1: 7,
    REC_DR2: 15,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 4.3,
    REC_AGE: 67.58333333,
  }, {
    firstName: 'Jillian',
    lastName: 'Harvey',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 24.7737,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 11,
    REC_A1: 29,
    REC_A2: 98,
    REC_B1: 7,
    REC_B2: 98,
    REC_DR1: 10,
    REC_DR2: 98,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 3.6,
    REC_AGE: 55.58333333,
  }, {
    firstName: 'Odo',
    lastName: 'Knowles',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.Hosp,
    REC_BMI: 36.3894,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.yes,
    REC_CREAT: 3.75,
    REC_A1: 3,
    REC_A2: 29,
    REC_B1: 7,
    REC_B2: 44,
    REC_DR1: 15,
    REC_DR2: 103,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 3.1,
    REC_AGE: 66.08333333,

  }, {
    firstName: 'Dack',
    lastName: 'Richards',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 38.5426,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 5.62,
    REC_A1: 2,
    REC_A2: 3,
    REC_B1: 62,
    REC_B2: 35,
    REC_DR1: 1,
    REC_DR2: 13,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 4.4,
    REC_AGE: 45,
  }, {
    firstName: 'John',
    lastName: 'Sheppard',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 33.2882,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 6.4,
    REC_A1: 1,
    REC_A2: 31,
    REC_B1: 7,
    REC_B2: 8,
    REC_DR1: 15,
    REC_DR2: 17,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 3.4,
    REC_AGE: 72.83333333,
  }, {
    firstName: 'Leonelle',
    lastName: 'Wallace',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.A,
    REC_MED_COND: medicalCondition.IntCareUnit,
    REC_BMI: 26.1719,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 5,
    REC_A1: 2,
    REC_A2: 11,
    REC_B1: 35,
    REC_B2: 57,
    REC_DR1: 1,
    REC_DR2: 13,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 3.3,
    REC_AGE: 45.08333333,
  }, {
    firstName: 'Braden ',
    lastName: 'Warren',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 30.5837,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 11.4,
    REC_A1: 1,
    REC_A2: 98,
    REC_B1: 8,
    REC_B2: 44,
    REC_DR1: 17,
    REC_DR2: 16,
    CAN_DIAB_TY: diabetesType.TI,
    CAN_TOT_ALBUMIN: 4.9,
    REC_AGE: 51.25,
  }, {
    firstName: 'Micheal',
    lastName: 'Terry',
    CAN_GENDER: gender.male,
    CAN_ABO: bloodType.B,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 24.809,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.UnDtr,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 6.8,
    REC_A1: 25,
    REC_A2: 66,
    REC_B1: 18,
    REC_B2: 38,
    REC_DR1: 11,
    REC_DR2: 15,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 4,
    REC_AGE: 71.41666667,
  }, {
    firstName: 'Jaran',
    lastName: 'Paige',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.O,
    REC_MED_COND: medicalCondition.NHosp,
    REC_BMI: 32.4305,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_MALIG: previousMalignancies.no,
    REC_CREAT: 11.3,
    REC_A1: 2,
    REC_A2: 11,
    REC_B1: 8,
    REC_B2: 35,
    REC_DR1: 103,
    REC_DR2: 7,
    CAN_DIAB_TY: diabetesType.no,
    CAN_TOT_ALBUMIN: 4.2,
    REC_AGE: 49.66666667,
  }, {
    firstName: 'Edward',
    lastName: 'Crawford',
    CAN_GENDER: gender.female,
    CAN_ABO: bloodType.AB,
    REC_MED_COND: medicalCondition.Hosp,
    REC_BMI: 28.4694,
    REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV.Pos,
    REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV.Neg,
    REC_MALIG: previousMalignancies.yes,
    REC_CREAT: 2.32,
    REC_A1: 2,
    REC_A2: 98,
    REC_B1: 44,
    REC_B2: 7,
    REC_DR1: 103,
    REC_DR2: 4,
    CAN_DIAB_TY: diabetesType.TII,
    CAN_TOT_ALBUMIN: 4.8,
    REC_AGE: 63.16666667,
  }];
  patients2: Patient[] = [];
  patients3: Patient[] = [];
  constructor() { }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
    this.patients3 = this.patients;
  }
  searchPatients() {
    this.patients2 = [];
    this.patients3 = this.patients;
    this.patients3.forEach(patient => {if (patient.firstName.includes(this.string) || patient.lastName.includes(this.string) ||
      ((patient.firstName + ' ' + patient.lastName).includes(this.string))) {
        this.patients2.push(patient);
        this.patients3 = this.patients2;
      } else if (!this.string) {
        this.patients3 = this.patients;
      }
                                       return patient;
    });
  }
}
