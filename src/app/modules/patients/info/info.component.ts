import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  patient: any = null;
  donor: any = null;
  potentials: {name: string, matchPercent: number, organLongevity: number, candidateSurvivalTime: number, color: string} [] = [
    {name: 'Ellie Daugherty', matchPercent: 95, organLongevity: 15, candidateSurvivalTime: 21, color: 'color: #0FE100'},
    {name: 'Reon Gilbert', matchPercent: 80, organLongevity: 12, candidateSurvivalTime: 17, color: 'color: #0DBC00'},
    {name: 'Diana Mcmahon', matchPercent: 71, organLongevity: 10, candidateSurvivalTime: 12, color: 'color: #B5E100'},
    {name: 'Affan Gillespie', matchPercent: 62, organLongevity: 9, candidateSurvivalTime: 8, color: 'color: #FFEE00'},
    {name: 'Liyana Porter', matchPercent: 54, organLongevity: 8, candidateSurvivalTime: 9, color: 'color: #FFD500'},
    {name: 'Kole Cervantes', matchPercent: 30, organLongevity: 16, candidateSurvivalTime: 8, color: 'color: #E1AD00'},
    {name: 'Raees Ray', matchPercent: 15, organLongevity: 21, candidateSurvivalTime: 10, color: 'color: #E18F00'},
    {name: 'Ailsa Gale', matchPercent: 10, organLongevity: 11, candidateSurvivalTime: 6, color: 'color: #E15A00'},
    {name: 'Mateo Couch', matchPercent: 2, organLongevity: 8, candidateSurvivalTime: 5, color: 'color: #FF0000'},
    ];
  constructor(private route: ActivatedRoute) {
    if (this.route.snapshot.queryParams.CAN_ABO) {
      this.patient = this.route.snapshot.queryParams;
    } else if (this.route.snapshot.queryParams.DON_ABO) {
      this.donor = this.route.snapshot.queryParams;
    }
  // console.log(this.route.snapshot.queryParams.DON_ABO);
  }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

  sortMatchPercent() {
    this.potentials.sort((a, b) => a.matchPercent < b.matchPercent ? 1 : -1);
  }
  sortOrganLongevity() {
    this.potentials.sort((a, b) => a.organLongevity < b.organLongevity ? 1 : -1);
  }
  sortCandidateSurvivalTime() {
    this.potentials.sort((a, b) => a.candidateSurvivalTime < b.candidateSurvivalTime ? 1 : -1);
  }
}
