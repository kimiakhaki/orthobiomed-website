import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-info-accardion',
  templateUrl: './info-accardion.component.html',
  styleUrls: ['./info-accardion.component.css']
})
export class InfoAccardionComponent implements OnInit {
  @Input() cardTitle;
  constructor() { }

  ngOnInit(): void {
  }

}
