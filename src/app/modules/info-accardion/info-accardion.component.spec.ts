import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoAccardionComponent } from './info-accardion.component';

describe('InfoAccardionComponent', () => {
  let component: InfoAccardionComponent;
  let fixture: ComponentFixture<InfoAccardionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoAccardionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoAccardionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
