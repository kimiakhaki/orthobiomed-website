import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import Swal from "sweetalert2";

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.css']
})
export class DoctorDashboardComponent implements OnInit {
  role = '';
  public barChartData: ChartDataSets[] = [
    { data: [98, 95, 90, 85, 80, 75, 70, 65, 50, 45, 40, 30, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], label: ''},
    { data: [98, 95, 90, 85, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 18, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], label: ''},
  ];
  public barChartLabels: Label[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18',
    '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];
  public barChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    plugins: {
      datalabels: {
        display: false,
      },
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
      ]
    },
    tooltips: {
      intersect: false,
      mode: 'point'
    },
    legend: {
      display: false,
      // labels: {
      //   // This more specific font property overrides the global property
      //   fontColor: 'red',
      //   fontSize: 18,
      // }
    },
    annotation: {
    },
  };
  public barChartColors: Color[] = [
    { // red
      // backgroundColor: '#267DFF',
      backgroundColor: '#1b1e25',
    },
    { // red
      backgroundColor: '#1daf9a',
    }
  ];
  public barChartLegend = false;
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [pluginAnnotations];

  // today: Date;
  // date: string;

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor() {
    this.role = localStorage.getItem('role');
    // this.today = new Date();
    // this.date = this.today.getFullYear() + '/' + (this.today.getMonth() + 1 ) + '/' + this.today.getDate();
  }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

}
