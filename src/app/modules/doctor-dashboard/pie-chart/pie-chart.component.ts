import { Component, OnInit } from '@angular/core';
import {ChartType, ChartOptions, ChartDataSets} from 'chart.js';
import {Label} from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {inspect} from 'util';


@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
// Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    aspectRatio: 1,
    legend: {
      position: 'top',
      labels: {
        fontColor: 'black',
      }
    },
    plugins: {
      datalabels: {
            anchor: 'center',
            color: 'white',
            font: {
              weight: 'bold',
              size: 14,
            },
        formatter: (value, ctx) => {
          // const label = ctx.chart.data.datasets[(ctx.dataIndex / 208) * 100].pe;
          // return label;
          let sum = 0;
          const dataArr = ctx.chart.data.datasets[0].data;
          // @ts-ignore
          dataArr.map(data => {
            sum += data;
          });
          const percentage = value + '(' + (value * 100 / sum).toFixed(2) + '%)';
          return percentage;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Candidates', 'Living Donors', 'Deceased Donors'];
  // public pieChartData: number[] = [157, 20, 31];
  public pieChartData: ChartDataSets[] = [{
    data: [[157, 20, 31]],
    // categoryPercentage: 20
    // barPercentage: 25
  }];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['#1b1e25', '#1daf9a', '#94b854'],
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
