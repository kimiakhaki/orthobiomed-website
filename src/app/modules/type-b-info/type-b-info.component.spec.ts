import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeBInfoComponent } from './type-b-info.component';

describe('TypeBInfoComponent', () => {
  let component: TypeBInfoComponent;
  let fixture: ComponentFixture<TypeBInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeBInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeBInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
