import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-type-b-info',
  templateUrl: './type-b-info.component.html',
  styleUrls: ['./type-b-info.component.css']
})
export class TypeBInfoComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
    Swal.fire({
      title: 'Please Wait',
      html: 'Loading data...',
      timer: 1000,
      timerProgressBar: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

}
