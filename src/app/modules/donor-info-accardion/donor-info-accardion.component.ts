import {Component, Input, OnInit} from '@angular/core';
import {IgG_IgM_EBV_HIV_HCV} from '../../objects/patient';

@Component({
  selector: 'app-donor-info-accardion',
  templateUrl: './donor-info-accardion.component.html',
  styleUrls: ['./donor-info-accardion.component.css']
})
export class DonorInfoAccardionComponent implements OnInit {
  @Input() cardTitle;
  @Input() donor;
  options: string[] = [];
  answers1: IgG_IgM_EBV_HIV_HCV;

  constructor() {
    for (let i = 0; i < 19; i++) {
      this.options.push(i.toString());
    }
    // if (typeof this.options !== 'number') {
    this.options.push('0101');
    // }
    this.options.push('0102');
    this.options.push('103');
    this.options.push('0301');
    this.options.push('0302');
    this.options.push('0401');
    this.options.push('0402');
    this.options.push('0404');
    this.options.push('0405');
    this.options.push('0407');
    this.options.push('0901');
    this.options.push('0902');
    this.options.push('1101');
    this.options.push('1104');
    this.options.push('1201');
    this.options.push('1202');
    this.options.push('1301');
    this.options.push('1303');
    this.options.push('1401');
    this.options.push('1402');
    this.options.push('1403');
    this.options.push('1404');
    this.options.push('1501');
    this.options.push('1502');
    this.options.push('1503');
    this.options.push('1601');
    this.options.push('1602');
  }

  ngOnInit(): void {
  }

}
