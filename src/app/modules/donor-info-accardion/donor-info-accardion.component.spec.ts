import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonorInfoAccardionComponent } from './donor-info-accardion.component';

describe('DonorInfoAccardionComponent', () => {
  let component: DonorInfoAccardionComponent;
  let fixture: ComponentFixture<DonorInfoAccardionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonorInfoAccardionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonorInfoAccardionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
