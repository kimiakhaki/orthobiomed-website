import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  role = '';
  constructor(private router: Router, private route: ActivatedRoute) {
    this.role = localStorage.getItem('role');
  }

  ngOnInit(): void {
  }

  toggleMenu() {
    const element = document.getElementById('menuBtnAlmostHidden');
    element.classList.toggle('change');
    const element2 = document.getElementById('sidebar');
    element2.classList.toggle('active');
    const element3 = document.getElementById('content');
    element3.classList.toggle('active');
    const element4 = document.getElementById('hideee');
    element4.classList.toggle('active');
    // element3.classList.toggle('hideKon');
  }
}
