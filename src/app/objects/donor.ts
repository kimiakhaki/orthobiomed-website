import {bloodType, gender, IgG_IgM_EBV_HIV_HCV, previousMalignancies} from './patient';

export class Donor {
  firstName: string;
  lastName: string;
  DON_INOTROP_AGENT_GE3: previousMalignancies;
  DON_CARDIAC_ARREST_AFTER_DEATH: previousMalignancies;
  DON_CONT_COCAINE: previousMalignancies;
  DON_BMI: number;
  DON_AGE_IN_MONTHS: number;
  DON_GENDER: gender;
  DON_ABO: bloodType;
  DON_ANTI_CMV: IgG_IgM_EBV_HIV_HCV;
  DON_ANTI_HCV: IgG_IgM_EBV_HIV_HCV;
  DON_A1: number;
  DON_A2: number;
  DON_B1: number;
  DON_B2: number;
  DON_DR1: number;
  DON_DR2: number;
  DON_TY: DON_TY;
  DON_HIST_HYPERTEN: DON_HIST_HYPERTEN;
}
export enum	DON_TY {
  lvng = 'Living',
  dceasd = 'Deceased',
}
export enum	DON_HIST_HYPERTEN {
  no = 'No',
  zeroToFive = 'Yes - 0 To 5 Years',
  sixToTen = 'Yes - 6 To 10 Years',
  Unkwn = 'Yes - Unknown Duration',
}
