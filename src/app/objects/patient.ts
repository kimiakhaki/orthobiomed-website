export class Patient {
  firstName: string;
  lastName: string;
  CAN_GENDER: gender;
  REC_AGE: number;
  REC_BMI: number;    //  16 < BMI < 56
  CAN_ABO: bloodType;
  REC_MED_COND: medicalCondition;
  CAN_DIAB_TY: diabetesType;
  REC_MALIG: previousMalignancies;
  // REC_DIAL_TY: preTransplantDialysis;
  REC_CREAT: number;    //  0 < PRA < 100
  REC_CMV_IGG: IgG_IgM_EBV_HIV_HCV;
  REC_CMV_IGM: IgG_IgM_EBV_HIV_HCV;
  CAN_TOT_ALBUMIN: number;
  REC_EBV_STAT: IgG_IgM_EBV_HIV_HCV;
  REC_HIV_STAT: IgG_IgM_EBV_HIV_HCV;
  REC_HCV_STAT: IgG_IgM_EBV_HIV_HCV;
  REC_A1: number;
  REC_A2: number;
  REC_B1: number;
  REC_B2: number;
  REC_DR1: number;
  REC_DR2: number;
}
export enum gender {
  female = 'Female',
  male = 'male'
}
export enum bloodType {
  // AbPlus = 'AB+',
  // AbMinus = 'A-',
  // APlus = 'A+',
  // AMinus = 'A-',
  // BPlus = 'B+',
  // BMinus = 'B-',
  // OPlus = 'O+',
  // OMinus = 'O-'
  AB = 'AB',
  A = 'A',
  B = 'B',
  O = 'O',
  A1B = 'A1B',
  A2B = 'A2B',
}
export enum medicalCondition {
  IntCareUnit = 'In Intensive Care Unit',
  Hosp = 'Hospitalized not in ICU',
  NHosp = 'Not Hospitalized'
}
export enum	diabetesType {
  TI = 'Type I',
  TII = 'Type II',
  OthrTs = 'Other types',
  Unkwn = 'Unknown',
  no = 'No'
}
export enum	previousMalignancies {
  yes = 'Yes',
  no = 'No',
  Unkwn = 'Unknown',
}

export enum	preTransplantDialysis {
  NoD = 'No dialysis',
  HemoD = 'Hemodialysis',
  PerD = 'Peritoneal Dialysis',
  Art = 'Continuous Arteriovenous Hemofiltration',
  Ven = 'Continuous Venous/Venous Hemofiltration',
  OthrDTs = 'Other Dialysis types'
}
export enum	IgG_IgM_EBV_HIV_HCV {
  Pos = 'Positive',
  Neg = 'Negative',
  UnDtr = 'Undetermined',
  NDn = 'NotDone',
}

