import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DefaultComponent} from './layouts/default/default.component';
import {DoctorDashboardComponent} from './modules/doctor-dashboard/doctor-dashboard.component';
import {PatientsComponent} from './modules/patients/patients.component';
import {DonorsComponent} from './modules/donors/donors.component';
import {InfoComponent} from './modules/patients/info/info.component';
import {DInfoComponent} from './modules/donors/d-info/d-info.component';
import {LoginComponent} from './modules/login/login.component';
import {FindADonorComponent} from './modules/looking-for-a-donor/find-a-donor.component';
import {InfoAccardionComponent} from './modules/info-accardion/info-accardion.component';
import {HaveADonorComponent} from './modules/have-a-donor/have-a-donor.component';
import {ViewResultsComponent} from './modules/have-a-donor/view-results/view-results.component';
import {DcdDashboardComponent} from './modules/dcd-dashboard/dcd-dashboard.component';
import {PatientRequestsComponent} from './modules/dcd-dashboard/patient-requests/patient-requests.component';
import {DoctorsComponent} from './modules/doctors/doctors.component';
import {TypeADashboardComponent} from './modules/type-a-dashboard/type-a-dashboard.component';
import {TypeAAccardionComponent} from './modules/type-a-accardion/type-a-accardion.component';
import {TypeBDashboardComponent} from './modules/type-b-dashboard/type-b-dashboard.component';
import {TypeBInfoComponent} from './modules/type-b-info/type-b-info.component';

const routes: Routes = [{
    path: 'login',
    component: LoginComponent
},
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
},
  {
    path: '',
    component: DefaultComponent,
  children: [
    {
    path: 'doctor-dashboard',
    component: DoctorDashboardComponent
  },
    {
      path: 'dashboard/candidates',
      component: PatientsComponent,
    },
    {
      path: 'dashboard/donors',
      component: DonorsComponent
    },
    {
      path: 'find-a-donor',
      component: FindADonorComponent,
      children: [{
        path: '',
        component: InfoAccardionComponent
      }]
    },
    {
      path: 'personal-info',
      component: TypeBInfoComponent,
    },
    {
      path: 'type-a-dashboard',
      component: TypeADashboardComponent,
      children: [{
        path: '',
        component: TypeAAccardionComponent
      }]
    },
    {
      path: 'type-b-dashboard',
      component: TypeBDashboardComponent,
    },
    {
      path: 'results',
      component: ViewResultsComponent,
    },
    // {
    //   path: 'dcd-dashboard',
    //   component: DcdDashboardComponent,
    // },
    // {
    //   path: 'patient-requests',
    //   component: DonorsComponent,
    // },
    // {
    //   path: 'dashboard/doctors',
    //   component: DoctorsComponent,
    // },
    {
      path: 'match-making',
      component: InfoComponent
    },
    {
      path: 'info',
      component: DInfoComponent,
      children: [{
        path: '',
        component: InfoAccardionComponent
      }]
    }
  ]
}];

@NgModule({
  declarations: [],
  imports: [
    // CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
