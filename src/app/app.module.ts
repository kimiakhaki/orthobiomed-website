import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {DefaultModule} from './layouts/default/default.module';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import { PatientRequestsComponent } from './modules/dcd-dashboard/patient-requests/patient-requests.component';
import { DoctorsComponent } from './modules/doctors/doctors.component';
import { TypeAAccardionComponent } from './modules/type-a-accardion/type-a-accardion.component';
import { TypeADashboardComponent } from './modules/type-a-dashboard/type-a-dashboard.component';
import { TypeBDashboardComponent } from './modules/type-b-dashboard/type-b-dashboard.component';
import { PatientInfoAccardionComponent } from './modules/patient-info-accardion/patient-info-accardion.component';
import { TypeBInfoComponent } from './modules/type-b-info/type-b-info.component';
import { DonorInfoAccardionComponent } from './modules/donor-info-accardion/donor-info-accardion.component';


@NgModule({
    declarations: [
        AppComponent,
    ],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    DefaultModule,
    BrowserAnimationsModule,
    FormsModule,
  ],
    providers: [],
    exports: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
